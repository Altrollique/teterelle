import 'dart:async';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:teterelle/app_router.dart';
import 'package:teterelle/locator.dart';
import 'package:teterelle/theme.dart';
import 'firebase_options.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  FirebaseDatabase.instance.setPersistenceEnabled(true);
  setup();

  // Adding ProviderScope enables Riverpod for the entire project
  // Adding our Logger to the list of observers
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getIt.allReady(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          print("GetIt ready");
          return MyAppLoaded();
        } else {
          print("Loading GetIt");
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}

class MyAppLoaded extends StatelessWidget {
  final _appRouter = getIt<AppRouter>();

  MyAppLoaded({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Téterelle',
      debugShowCheckedModeBanner: false,
      theme: appTheme,
      routerDelegate: _appRouter.delegate(),
      routeInformationParser: _appRouter.defaultRouteParser(),
    );
  }
}
