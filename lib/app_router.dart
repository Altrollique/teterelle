import 'package:auto_route/auto_route.dart';
import 'package:teterelle/app_router.gr.dart';
import 'package:teterelle/managers/auth_manager.dart';

@AutoRouterConfig(replaceInRouteName: 'Page,Route')
class AppRouter extends $AppRouter implements AutoRouteGuard {
  final AuthManager authManager;

  AppRouter(this.authManager);

  @override
  List<AutoRoute> get routes => [
        /// routes go here
        AutoRoute(
          page: LoginRoute.page,
        ),
        AutoRoute(
          page: RegisterRoute.page,
        ),
        AutoRoute(
          page: HomeRoute.page,
          initial: true,
        ),
        AutoRoute(
          page: AddLunchRoute.page,
        ),
        AutoRoute(
          page: AddChangeRoute.page,
        ),
        AutoRoute(
          page: AddBathRoute.page,
        ),
        AutoRoute(
          page: ChildrenRoute.page,
        ),
        AutoRoute(
          page: ChildRoute.page,
        ),
      ];

  @override
  Future<void> onNavigation(
      NavigationResolver resolver, StackRouter router) async {
    if (authManager.isAuth.value) {
      print("Guard : we are Authed");
      // we continue navigation
      resolver.next();
    } else if (resolver.route.name == LoginRoute.name || resolver.route.name == RegisterRoute.name) {
      print("Guard : we are on a page that doesn't require login");
      // we continue navigation
      resolver.next();
    } else {
      // else we navigate to the Login page so we get authenticateed
      print("Guard : not logged in, we go to login page");
      resolver.next(false);
      replaceAll([LoginRoute()]);
    }
  }
}
