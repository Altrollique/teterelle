import 'package:get_it/get_it.dart';
import 'package:teterelle/app_router.dart';
import 'package:teterelle/managers/app_data_manager.dart';
import 'package:teterelle/managers/auth_manager.dart';
import 'package:teterelle/managers/session_manager.dart';
import 'package:teterelle/services/app_data_service.dart';
import 'package:teterelle/services/auth_service.dart';
import 'package:teterelle/services/child_service.dart';
import 'package:teterelle/services/crypto_service.dart';
import 'package:teterelle/services/database_service.dart';
import 'package:teterelle/services/local_storage_service.dart';
import 'package:teterelle/services/serialization_service.dart';

final getIt = GetIt.instance;

void setup() {
  //AppDataManager
  getIt.registerSingleton<AppDataManager>(AppDataManager());
  //AuthManager
  getIt.registerSingleton<AuthManager>(AuthManager());
  //SessionManager
  getIt.registerSingleton<SessionManager>(SessionManager());
  //CryptoService
  getIt.registerSingleton<CryptoService>(CryptoService());
  //SerializationService
  getIt.registerSingleton<SerializationService>(SerializationService());

  //LocalStorageService
  getIt.registerSingletonAsync<LocalStorageService>(
      () async => await LocalStorageService().init());

  //DatabaseService
  getIt.registerSingleton<DatabaseService>(
    DatabaseService(
      getIt<CryptoService>(),
      getIt<SerializationService>(),
    ),
  );

  //AppDataService
  getIt.registerSingletonAsync<AppDataService>(
    () async => await AppDataService(
      getIt<DatabaseService>(),
      getIt<AuthManager>(),
      getIt<AppDataManager>(),
    ).init(),
  );

  //ChildService
  getIt.registerSingletonWithDependencies<ChildService>(
    () => ChildService(
      getIt<AppDataManager>(),
      getIt<AppDataService>(),
    ),
    dependsOn: [
      AppDataService,
    ],
  );

  //AuthService
  getIt.registerSingletonAsync<AuthService>(
    () async => await AuthService(
      getIt<AuthManager>(),
      getIt<AppDataManager>(),
      getIt<AppDataService>(),
      getIt<LocalStorageService>(),
      getIt<DatabaseService>(),
    ).init(),
    dependsOn: [
      AppDataService,
      LocalStorageService,
    ],
  );

  //AppRouter
  getIt.registerSingleton<AppRouter>(
    AppRouter(
      getIt<AuthManager>(),
    ),
  );
}
