import 'package:flutter/material.dart';

final appTheme = ThemeData(
  colorSchemeSeed: Colors.blue.shade800,
  useMaterial3: true
);
