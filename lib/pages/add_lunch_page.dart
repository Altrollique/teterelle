import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:teterelle/app_router.dart';
import 'package:teterelle/app_router.gr.dart';
import 'package:teterelle/locator.dart';
import 'package:teterelle/models/enums/lunch_type.dart';
import 'package:teterelle/models/lunch_item.dart';
import 'package:teterelle/services/app_data_service.dart';
import 'package:teterelle/services/child_service.dart';
import 'package:teterelle/widgets/date_picker.dart';
import 'package:teterelle/widgets/time_picker.dart';
import 'package:teterelle/widgets/toggle_side.dart';
import 'package:teterelle/widgets/validate_action_button.dart';
import 'package:uuid/uuid.dart';

@RoutePage()
class AddLunchPage extends StatefulWidget {
  const AddLunchPage({super.key, this.lunchItem});

  static String get routeName => 'add-lunch';

  static String get routeLocation => '/$routeName';
  final LunchItem? lunchItem;

  @override
  State<AddLunchPage> createState() => _AddLunchPageState();
}

class _AddLunchPageState extends State<AddLunchPage> {
  final AppRouter appRouter = getIt<AppRouter>();
  final AppDataService appDataService = getIt<AppDataService>();
  final ChildService childService = getIt<ChildService>();

  bool edit = false;
  String uuid = Uuid().v4();
  DateTime date = DateTime.now();
  int duration = 10;
  int biberonMl = 0;
  bool isLeft = true;
  LunchType lunchType = LunchType.feeding;

  @override
  void initState() {
    super.initState();
    edit = widget.lunchItem != null;
    uuid = widget.lunchItem?.uuid ?? Uuid().v4();
    date = widget.lunchItem?.date ?? DateTime.now();
    duration = widget.lunchItem?.duration ?? 10;
    biberonMl = widget.lunchItem?.biberonMl ?? 0;
    isLeft = widget.lunchItem?.isLeft ?? true;
    lunchType = widget.lunchItem?.lunchType ?? LunchType.feeding;
  }

  void handleDateChange(DateTime newValue) {
    setState(() {
      date = newValue;
    });
  }

  void handleDurationChange(int newValue) {
    setState(() {
      duration = newValue;
    });
  }

  void handleBiberonMlChange(int newValue) {
    setState(() {
      biberonMl = newValue;
    });
  }

  void handleSideChange(bool newValue) {
    setState(() {
      isLeft = newValue;
    });
  }

  void handleIsBiberonChange(bool newValue) {
    setState(() {
      lunchType = newValue ? LunchType.feeding : LunchType.bottle;
    });
  }

  Future<void> handleValidate() async {
    print("start handleValidate");
    await appDataService.readAppDataFromDB();

    LunchItem item =
        LunchItem(uuid, date, lunchType, duration, isLeft, biberonMl);
    if (edit) {
      childService.editHistory(uuid, item);
    } else {
      childService.addHistory(item);
    }
    await appDataService.writeAppDataToDB();
    appRouter.navigate(HomeRoute());
    print("end handleValidate change");
  }

  renderConditionalData() {
    switch (lunchType) {
      case LunchType.feeding:
        return renderFeeding();
      case LunchType.bottle:
        return renderBottle();
    }
  }

  renderFeeding() {
    return [
      ToggleSide(
        value: isLeft,
        onChanged: handleSideChange,
      ),
      SizedBox(
        height: 20,
      ),
      Text("Durée : "),
      NumberPicker(
        value: duration,
        minValue: 0,
        maxValue: 30,
        step: 1,
        axis: Axis.horizontal,
        onChanged: handleDurationChange,
      ),
    ];
  }

  renderBottle() {
    return [
      Text("Quantité bue (mL) : "),
      NumberPicker(
        value: biberonMl,
        minValue: 0,
        maxValue: 300,
        step: 1,
        axis: Axis.horizontal,
        onChanged: handleBiberonMlChange,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Ajouter une tétée"),
          actions: [
            IconButton(
                onPressed: () => appRouter.navigate(HomeRoute()),
                icon: Icon(Icons.close))
          ],
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ToggleSide(
                value: lunchType == LunchType.feeding,
                onChanged: handleIsBiberonChange,
                leftLabel: "Tétée",
                rightLabel: "Biberon",
              ),
              SizedBox(
                height: 20,
              ),
              DatePicker(
                value: date,
                onChanged: handleDateChange,
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("Fin : "),
                  SizedBox(
                    width: 10,
                  ),
                  TimePicker(
                    value: date,
                    onChanged: handleDateChange,
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              ...renderConditionalData(),
            ],
          ),
        ),
        floatingActionButton: ValidateActionButton(
          action: handleValidate,
        ),
      ),
    );
  }
}
