import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:get_it_mixin/get_it_mixin.dart';
import 'package:teterelle/app_router.dart';
import 'package:teterelle/app_router.gr.dart';
import 'package:teterelle/locator.dart';
import 'package:teterelle/managers/app_data_manager.dart';
import 'package:teterelle/managers/session_manager.dart';
import 'package:teterelle/models/app_data.dart';
import 'package:teterelle/services/app_data_service.dart';
import 'package:teterelle/widgets/alert_dialog.dart';

@RoutePage()
class ChildrenPage extends StatelessWidget with GetItMixin {
  final AppRouter appRouter = getIt<AppRouter>();
  final SessionManager sessionManager = getIt<SessionManager>();
  final AppDataService appDataService = getIt<AppDataService>();

  ChildrenPage({super.key});

  static String get routeName => 'children';

  static String get routeLocation => '/children';

  void handleAddClick(BuildContext context) {
    appRouter.push(ChildRoute(childId: "new"));
  }

  void handleUpdateClick(BuildContext context, String id) {
    appRouter.push(ChildRoute(childId: id));
  }

  Future<void> deleteElement(String id) async {
    print("handleDeleteClick");
    sessionManager.globalLoading = true;
    try {
      await appDataService.readAppDataFromDB();
      appDataService.removeChild(id);
      await appDataService.writeAppDataToDB();
    } catch (error) {
      print("Error while trying to delete the child $id");
      print(error);
    } finally {
      sessionManager.globalLoading = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    final children = watchOnly((AppDataManager m) => m.children);
    final loading = watchOnly((SessionManager s) => s.globalLoading);

    void handleDeleteElement(String id) async {
      AppAlertDialog alert = AppAlertDialog(
        title: "Suppression d'élément",
        content: "Êtes-vous sûr•e de vouloir supprimer cet élément ?",
        continueCallBack: () {
          print("Close alert");
          Navigator.of(context).pop();
        },
        buttons: [
          AlertButton(
            label: "Oui",
            onPressed: () => deleteElement(id),
          ),
          AlertButton(
            label: "Non",
          ),
        ],
      );
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }

    Widget renderDeleteButton(String id) {
      if (children.length < 2) {
        return SizedBox(
          width: 48,
          height: 48,
        );
      } else {
        return IconButton(
          onPressed: () => handleDeleteElement(id),
          icon: Icon(Icons.delete),
        );
      }
    }

    Widget renderBody() {
      if (loading) {
        return Center(
          child: CircularProgressIndicator(),
        );
      } else {
        return Column(
          children: [
            SizedBox(
              height: 20,
            ),
            for (var child in children)
              Card(
                color: Theme.of(context).cardColor,
                child: InkWell(
                  onTap: () => handleUpdateClick(context, child.id),
                  child: Row(
                    children: [
                      Icon(Icons.child_care),
                      Spacer(flex: 2),
                      Text(child.name),
                      Spacer(flex: 2),
                      renderDeleteButton(child.id),
                    ],
                  ),
                ),
              ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              child: IconButton(
                onPressed: () => handleAddClick(context),
                icon: Icon(Icons.add),
              ),
            ),
          ],
        );
      }
    }

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Mes enfants :"),
          backgroundColor: Theme.of(context).colorScheme.surface,
          scrolledUnderElevation: 0.0,
          actions: [
            IconButton(
              onPressed: () => appRouter.navigate(HomeRoute()),
              icon: Icon(Icons.close),
            ),
          ],
        ),
        body: renderBody(),
      ),
    );
  }
}
