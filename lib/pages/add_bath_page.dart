import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:teterelle/app_router.dart';
import 'package:teterelle/app_router.gr.dart';
import 'package:teterelle/locator.dart';
import 'package:teterelle/models/bath_item.dart';
import 'package:teterelle/services/app_data_service.dart';
import 'package:teterelle/services/child_service.dart';
import 'package:teterelle/widgets/date_picker.dart';
import 'package:teterelle/widgets/time_picker.dart';
import 'package:teterelle/widgets/validate_action_button.dart';
import 'package:uuid/uuid.dart';

@RoutePage()
class AddBathPage extends StatefulWidget {
  const AddBathPage({super.key, this.bathItem});

  static String get routeName => 'add-bath';

  static String get routeLocation => '/$routeName';
  final BathItem? bathItem;

  @override
  State<AddBathPage> createState() => _AddBathPageState();
}

class _AddBathPageState extends State<AddBathPage> {
  final AppRouter appRouter = getIt<AppRouter>();
  final AppDataService appDataService = getIt<AppDataService>();
  final ChildService childService = getIt<ChildService>();

  bool edit = false;
  String uuid = Uuid().v4();
  DateTime date = DateTime.now();

  @override
  void initState() {
    super.initState();
    edit = widget.bathItem != null;
    uuid = widget.bathItem?.uuid ?? Uuid().v4();
    date = widget.bathItem?.date ?? DateTime.now();
  }

  void handleDateChange(DateTime newValue) {
    setState(() {
      date = newValue;
    });
  }

  Future<void> handleValidate() async {
    print("start handleValidate bath");
    await appDataService.readAppDataFromDB();

    BathItem item = BathItem(uuid, date);
    if (edit) {
      childService.editHistory(uuid, item);
    } else {
      childService.addHistory(item);
    }
    await appDataService.writeAppDataToDB();
    appRouter.navigate(HomeRoute());
    print("end handleValidate bath");
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Ajouter un bain"),
          actions: [
            IconButton(
                onPressed: () => appRouter.navigate(HomeRoute()),
                icon: Icon(Icons.close))
          ],
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Spacer(flex: 2),
              DatePicker(
                value: date,
                onChanged: handleDateChange,
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("Heure : "),
                  SizedBox(
                    width: 10,
                  ),
                  TimePicker(
                    value: date,
                    onChanged: handleDateChange,
                  ),
                ],
              ),
              Spacer(flex: 2),
            ],
          ),
        ),
        floatingActionButton: ValidateActionButton(
          action: handleValidate,
        ),
      ),
    );
  }
}
