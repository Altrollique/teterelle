import 'package:auto_route/auto_route.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:teterelle/app_router.dart';
import 'package:teterelle/app_router.gr.dart';
import 'package:teterelle/exceptions/app_login_exception.dart';
import 'package:teterelle/locator.dart';
import 'package:teterelle/models/app_data.dart';
import 'package:teterelle/services/auth_service.dart';

@RoutePage()
class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  static String get routeName => 'register';

  static String get routeLocation => '/$routeName';

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final AuthService authService = getIt<AuthService>();
  final AppRouter appRouter = getIt<AppRouter>();

  final _formKey = GlobalKey<FormState>();
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController passwordConfirmController = TextEditingController();
  bool loading = false;

  Future<void> handleRegister() async {
    print("handleRegister");
    setState(() {
      loading = true;
    });
    try {
      if (_formKey.currentState!.validate()) {
        print("Form is valid");
        var username = usernameController.value.text.trim();
        final alphanumeric = RegExp(r'^[a-zA-Z0-9]+$');
        var password = passwordController.value.text.trim();
        var passwordConfirm = passwordConfirmController.value.text.trim();
        if (password != passwordConfirm) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
                content: Text('Les mots de passe ne correspondent pas')),
          );
          return;
        }
        if (!alphanumeric.hasMatch(username)) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text(
                  "Veuillez n'utiliser que des lettres et des chiffres pour l'identifiant."),
            ),
          );
          return;
        }
        print("Passwords are identic");
        await authService.register(username, password);
        appRouter.replaceAll([HomeRoute()]);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Veuillez renseigner tout les champs')),
        );
      }
    } on AppAuthException catch (error) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text(error.cause)),
      );
    } catch (error) {
      print("Error while handleRegister");
      print(error);
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Une erreur est survenue.')),
      );
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    print("Dispose RegisterPage");
    usernameController.text = "";
    passwordController.text = "";
    passwordConfirmController.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Inscription"),
      ),
      body: Center(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                  child: TextFormField(
                    controller: usernameController,
                    decoration: const InputDecoration(
                        border: OutlineInputBorder(), labelText: "Identifiant"),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Veuillez entrer votre identifiant';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                  child: TextFormField(
                    controller: passwordController,
                    obscureText: true,
                    decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: "Mot de passe"),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Veuillez entrer votre mot de passe';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                  child: TextFormField(
                    controller: passwordConfirmController,
                    obscureText: true,
                    decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: "Confirmation"),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Confirmez votre mot de passe';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 16.0),
                  child: Center(
                    child: ElevatedButton(
                      onPressed: handleRegister,
                      child: const Text("Inscription"),
                    ),
                  ),
                ),
                Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 8, vertical: 16.0),
                  child: Center(
                    child: ElevatedButton(
                      onPressed: () => appRouter.replaceAll([LoginRoute()]),
                      child: const Text("Connexion"),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
