import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:teterelle/app_router.dart';
import 'package:teterelle/app_router.gr.dart';
import 'package:teterelle/exceptions/app_login_exception.dart';
import 'package:teterelle/locator.dart';
import 'package:teterelle/services/auth_service.dart';

@RoutePage()
class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  static String get routeName => 'login';

  static String get routeLocation => '/$routeName';

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final AuthService authService = getIt<AuthService>();
  final AppRouter appRouter = getIt<AppRouter>();

  final _formKey = GlobalKey<FormState>();
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool loading = false;

  Future<void> handleLogin() async {
    setState(() {
      loading = true;
    });
    try {
      print("Start login");
      if (_formKey.currentState!.validate()) {
        print("Form is valid");
        var username = usernameController.value.text.trim();
        var password = passwordController.value.text.trim();
        await authService.login(username, password);
        appRouter.replaceAll([HomeRoute()]);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Veuillez renseigner tout les champs')),
        );
      }
    } on AppAuthException catch (error) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text(error.cause)),
      );
    } catch (error) {
      print("Error while handleLogin");
      print(error);
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Une erreur est survenue.')),
      );
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    print("Dispose LoginPage");
    usernameController.text = "";
    passwordController.text = "";
  }

  @override
  Widget build(BuildContext context) {
    renderButtons() {
      if (loading) {
        return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: CircularProgressIndicator());
      } else {
        return Column(
          children: [
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 8, vertical: 16.0),
              child: Center(
                child: ElevatedButton(
                  onPressed: handleLogin,
                  child: const Text('Connexion'),
                ),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 8, vertical: 16.0),
              child: Center(
                child: ElevatedButton(
                  onPressed: () => appRouter.replaceAll([RegisterRoute()]),
                  child: const Text("S'inscrire"),
                ),
              ),
            ),
          ],
        );
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Connexion"),
      ),
      body: Center(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                  child: TextFormField(
                    controller: usernameController,
                    decoration: const InputDecoration(
                        border: OutlineInputBorder(), labelText: "Identifiant"),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Veuillez renseigner votre identifiant';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                  child: TextFormField(
                    controller: passwordController,
                    obscureText: true,
                    decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: "Mot de passe"),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Veuillez renseigner votre mot de passe';
                      }
                      return null;
                    },
                  ),
                ),
                renderButtons(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
