import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:get_it_mixin/get_it_mixin.dart';
import 'package:teterelle/app_router.dart';
import 'package:teterelle/app_router.gr.dart';
import 'package:teterelle/locator.dart';
import 'package:teterelle/managers/app_data_manager.dart';
import 'package:teterelle/managers/session_manager.dart';
import 'package:teterelle/models/bath_item.dart';
import 'package:teterelle/models/change_item.dart';
import 'package:teterelle/models/enums/history_item_type.dart';
import 'package:teterelle/models/history_item.dart';
import 'package:teterelle/models/lunch_item.dart';
import 'package:teterelle/services/app_data_service.dart';
import 'package:teterelle/services/child_service.dart';
import 'package:teterelle/widgets/alert_dialog.dart';
import 'package:teterelle/widgets/app_header.dart';
import 'package:teterelle/widgets/child_tabs.dart';
import 'package:teterelle/widgets/history_element.dart';
import 'package:teterelle/widgets/last_event_card.dart';

@RoutePage()
class HomePage extends StatefulWidget with GetItStatefulWidgetMixin {
  HomePage({super.key});

  static String get routeName => 'home';

  static String get routeLocation => '/';

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with GetItStateMixin {
  final AppRouter appRouter = getIt<AppRouter>();

  final SessionManager sessionManager = getIt<SessionManager>();

  final AppDataService appDataService = getIt<AppDataService>();

  final AppDataManager appDataManager = getIt<AppDataManager>();

  final ChildService childService = getIt<ChildService>();

  @override
  void initState() {
    super.initState();
    sessionManager.globalLoading = true;
    appDataService
        .readAppDataFromDB()
        .then((value) => sessionManager.globalLoading = false);
  }

  Future<void> deleteElement(String uuid) async {
    sessionManager.globalLoading = true;
    try {
      await appDataService.readAppDataFromDB();
      childService.removeHistory(uuid);
      appDataService.updateSelectedChild(appDataManager.child);

      await appDataService.writeAppDataToDB();
    } catch (error) {
      print("error while deleting element");
      print(error);
    } finally {
      sessionManager.globalLoading = false;
    }
  }

  Future<void> handleRefresh() async {
    await appDataService.readAppDataFromDB();
  }

  @override
  Widget build(BuildContext context) {
    final child = watchOnly((AppDataManager m) => m.child);
    final historyState = child.history.reversed;
    final loading = watchOnly((SessionManager s) => s.globalLoading);
    HistoryItem? lastLunch = historyState.isNotEmpty
        ? historyState.firstWhere(
            (element) => element.type == HistoryItemType.lunch,
            orElse: () => historyState.last)
        : null;
    HistoryItem? lastChange = historyState.isNotEmpty
        ? historyState.firstWhere(
            (element) =>
                element.type == HistoryItemType.change ||
                element.type == HistoryItemType.bath,
            orElse: () => historyState.last)
        : null;
    HistoryItem? lastBath = historyState.isNotEmpty
        ? historyState.firstWhere(
            (element) => element.type == HistoryItemType.bath,
            orElse: () => historyState.last)
        : null;

    void handleDeleteElement(String uuid) async {
      AppAlertDialog alert = AppAlertDialog(
        title: "Suppression d'élément",
        content: "Êtes-vous sûr•e de vouloir supprimer cet élément ?",
        continueCallBack: () {
          print("Close alert");
          Navigator.of(context).pop();
        },
        buttons: [
          AlertButton(
            label: "Oui",
            onPressed: () => deleteElement(uuid),
          ),
          AlertButton(
            label: "Non",
          ),
        ],
      );
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }

    Widget renderBody() {
      if (loading) {
        return Center(
          child: CircularProgressIndicator(),
        );
      } else {
        return RefreshIndicator(
          onRefresh: handleRefresh,
          child: Column(children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: Wrap(
                alignment: WrapAlignment.spaceEvenly,
                children: [
                  LastEventCard(
                    from: lastLunch?.date ?? DateTime.now(),
                    size: 120,
                    icon: lastLunch?.icon ?? LunchItem.defaultIcon,
                    backgroundColor: lastLunch?.color ?? LunchItem.defaultColor,
                    onTap: () => appRouter.push(AddLunchRoute()),
                  ),
                  LastEventCard(
                    from: lastChange?.date ?? DateTime.now(),
                    size: 120,
                    icon: ChangeItem.defaultIcon,
                    backgroundColor: ChangeItem.defaultColor,
                    onTap: () => appRouter.push(AddChangeRoute()),
                  ),
                  LastEventCard(
                    from: lastBath?.date ?? DateTime.now(),
                    size: 120,
                    icon: BathItem.defaultIcon,
                    backgroundColor: BathItem.defaultColor,
                    onTap: () => appRouter.push(AddBathRoute()),
                  ),
                ],
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: historyState.length,
                itemBuilder: (context, index) {
                  return HistoryElement(
                    historyItem: historyState.elementAt(index),
                    onDelete: handleDeleteElement,
                  );
                },
              ),
            ),
          ]),
        );
      }
    }

    print("Draw home");
    return SafeArea(
      child: Scaffold(
          appBar: AppHeader(),
          body: renderBody(),
          bottomNavigationBar: ChildTabs()),
    );
  }
}
