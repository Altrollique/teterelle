import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:teterelle/app_router.dart';
import 'package:teterelle/app_router.gr.dart';
import 'package:teterelle/locator.dart';
import 'package:teterelle/managers/app_data_manager.dart';
import 'package:teterelle/models/child.dart';
import 'package:teterelle/services/app_data_service.dart';
import 'package:teterelle/widgets/app_primary_button.dart';

@RoutePage()
class ChildPage extends StatefulWidget {
  const ChildPage({super.key, required this.childId});

  static String get routeName => 'childById';

  static String get routeLocation => '/child/:child_id';
  final String childId;

  @override
  State<ChildPage> createState() => _ChildPageState();
}

class _ChildPageState extends State<ChildPage> {
  final AppRouter appRouter = getIt<AppRouter>();
  final AppDataService appDataService = getIt<AppDataService>();
  final AppDataManager appDataManager = getIt<AppDataManager>();

  final _formKey = GlobalKey<FormState>();
  TextEditingController childNameController = TextEditingController();
  bool loading = false;
  String buttonLabel = "Ajouter";
  String title = "Ajouter un enfant";

  @override
  void initState() {
    super.initState();
    Child? child = appDataService.getChildById(widget.childId);
    if (child == null) {
      childNameController.text = "Bébé";
      buttonLabel = "Ajouter";
      title = "Ajouter un enfant";
    } else {
      childNameController.text = child.name;
      buttonLabel = "Modifier";
      title = "Modifier un enfant";
    }
  }

  Future<void> handleValidate() async {
    print("handleValidate");
    await appDataService.readAppDataFromDB();

    print("get child from app data");
    Child child = appDataService.getChildByChildId(widget.childId);
    print("Old child name : ${child.name}");
    child.name = childNameController.value.text;
    print("New child name : ${child.name}");
    print("controller value : ${childNameController.value.text}");

    int childIndex;
    if (widget.childId == "new") {
      print("add child to app data");
      appDataService.addChild(child);
      childIndex = appDataService.getChildIndexByChildId(child.id);
    } else {
      print("update child from app data");
      childIndex = appDataService.getChildIndexByChildId(child.id);
      appDataService.updateChildById(child, childIndex);
    }

    await appDataService.writeAppDataToDB();
    appDataManager.childIndex = childIndex;
    appRouter.navigate(HomeRoute());
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(title),
          backgroundColor: Theme.of(context).colorScheme.surface,
          scrolledUnderElevation: 0.0,
          actions: [
            IconButton(
              onPressed: () => appRouter.navigate(ChildrenRoute()),
              icon: Icon(Icons.close),
            ),
          ],
        ),
        body: Center(
          child: Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                    child: TextFormField(
                      controller: childNameController,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: "Nom de l'enfant"),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Veuillez renseigner le nom de l'enfant";
                        }
                        return null;
                      },
                    ),
                  ),
                  AppPrimaryButon(
                    label: buttonLabel,
                    onTap: handleValidate,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
