import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:teterelle/app_router.dart';
import 'package:teterelle/app_router.gr.dart';
import 'package:teterelle/locator.dart';
import 'package:teterelle/models/change_item.dart';
import 'package:teterelle/services/app_data_service.dart';
import 'package:teterelle/services/child_service.dart';
import 'package:teterelle/widgets/date_picker.dart';
import 'package:teterelle/widgets/time_picker.dart';
import 'package:teterelle/widgets/validate_action_button.dart';
import 'package:uuid/uuid.dart';

@RoutePage()
class AddChangePage extends StatefulWidget {
  const AddChangePage({super.key, this.changeItem});

  static String get routeName => 'add-change';

  static String get routeLocation => '/$routeName';
  final ChangeItem? changeItem;

  @override
  State<AddChangePage> createState() => _AddChangePageState();
}

class _AddChangePageState extends State<AddChangePage> {
  final AppRouter appRouter = getIt<AppRouter>();
  final AppDataService appDataService = getIt<AppDataService>();
  final ChildService childService = getIt<ChildService>();

  bool edit = false;
  String uuid = Uuid().v4();
  DateTime date = DateTime.now();
  bool isPee = false;
  bool isPoop = false;
  bool isVitamin = false;
  bool isCord = false;

  @override
  void initState() {
    super.initState();
    edit = widget.changeItem != null;
    uuid = widget.changeItem?.uuid ?? Uuid().v4();
    date = widget.changeItem?.date ?? DateTime.now();
    isPee = widget.changeItem?.isPee ?? false;
    isPoop = widget.changeItem?.isPoop ?? false;
    isVitamin = widget.changeItem?.isVitamin ?? false;
    isCord = widget.changeItem?.isCord ?? false;
  }

  void handleDateChange(DateTime newValue) {
    setState(() {
      date = newValue;
    });
  }

  void handleIsPeeChange(bool newValue) {
    setState(() {
      isPee = newValue;
    });
  }

  void handleIsPoopChange(bool newValue) {
    setState(() {
      isPoop = newValue;
    });
  }

  void handleIsVitaminChange(bool newValue) {
    setState(() {
      isVitamin = newValue;
    });
  }

  void handleIsCordChange(bool newValue) {
    setState(() {
      isCord = newValue;
    });
  }

  Future<void> handleValidate() async {
    print("start handleValidate change");
    await appDataService.readAppDataFromDB();

    ChangeItem item = ChangeItem(uuid, date, isPee, isPoop, isVitamin, isCord);
    if (edit) {
      childService.editHistory(uuid, item);
    } else {
      childService.addHistory(item);
    }
    await appDataService.writeAppDataToDB();
    appRouter.navigate(HomeRoute());
    print("end handleValidate change");
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text("Ajouter un change"),
          actions: [
            IconButton(
                onPressed: () => appRouter.navigate(HomeRoute()),
                icon: Icon(Icons.close))
          ],
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("Date : "),
                  SizedBox(
                    width: 10,
                  ),
                  DatePicker(
                    value: date,
                    onChanged: handleDateChange,
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("Heure : "),
                  SizedBox(
                    width: 10,
                  ),
                  TimePicker(
                    value: date,
                    onChanged: handleDateChange,
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              ElevatedButton.icon(
                onPressed: () {
                  handleIsPeeChange(!isPee);
                },
                label: Text(
                  "Pipi",
                ),
                icon: Icon(isPee
                    ? Icons.check_box_outlined
                    : Icons.check_box_outline_blank_outlined),
                style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.all(10),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              ElevatedButton.icon(
                onPressed: () {
                  handleIsPoopChange(!isPoop);
                },
                label: Text(
                  "Caca",
                ),
                icon: Icon(isPoop
                    ? Icons.check_box_outlined
                    : Icons.check_box_outline_blank_outlined),
                style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.all(10),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              ElevatedButton.icon(
                onPressed: () {
                  handleIsVitaminChange(!isVitamin);
                },
                label: Text(
                  "Vitamines",
                ),
                icon: Icon(isVitamin
                    ? Icons.check_box_outlined
                    : Icons.check_box_outline_blank_outlined),
                style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.all(10),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              ElevatedButton.icon(
                onPressed: () {
                  handleIsCordChange(!isCord);
                },
                label: Text(
                  "Nettoyage du cordon",
                ),
                icon: Icon(isCord
                    ? Icons.check_box_outlined
                    : Icons.check_box_outline_blank_outlined),
                style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.all(10),
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: ValidateActionButton(
          action: handleValidate,
        ),
      ),
    );
  }
}
