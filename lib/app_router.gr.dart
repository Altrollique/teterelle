// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i9;
import 'package:flutter/material.dart' as _i10;
import 'package:teterelle/models/bath_item.dart' as _i11;
import 'package:teterelle/models/change_item.dart' as _i13;
import 'package:teterelle/models/lunch_item.dart' as _i12;
import 'package:teterelle/pages/add_bath_page.dart' as _i2;
import 'package:teterelle/pages/add_change_page.dart' as _i5;
import 'package:teterelle/pages/add_lunch_page.dart' as _i4;
import 'package:teterelle/pages/child_page.dart' as _i8;
import 'package:teterelle/pages/children_page.dart' as _i3;
import 'package:teterelle/pages/home_page.dart' as _i7;
import 'package:teterelle/pages/login_page.dart' as _i6;
import 'package:teterelle/pages/register_page.dart' as _i1;

abstract class $AppRouter extends _i9.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i9.PageFactory> pagesMap = {
    RegisterRoute.name: (routeData) {
      return _i9.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i1.RegisterPage(),
      );
    },
    AddBathRoute.name: (routeData) {
      final args = routeData.argsAs<AddBathRouteArgs>(
          orElse: () => const AddBathRouteArgs());
      return _i9.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i2.AddBathPage(
          key: args.key,
          bathItem: args.bathItem,
        ),
      );
    },
    ChildrenRoute.name: (routeData) {
      final args = routeData.argsAs<ChildrenRouteArgs>(
          orElse: () => const ChildrenRouteArgs());
      return _i9.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i3.ChildrenPage(key: args.key),
      );
    },
    AddLunchRoute.name: (routeData) {
      final args = routeData.argsAs<AddLunchRouteArgs>(
          orElse: () => const AddLunchRouteArgs());
      return _i9.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i4.AddLunchPage(
          key: args.key,
          lunchItem: args.lunchItem,
        ),
      );
    },
    AddChangeRoute.name: (routeData) {
      final args = routeData.argsAs<AddChangeRouteArgs>(
          orElse: () => const AddChangeRouteArgs());
      return _i9.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i5.AddChangePage(
          key: args.key,
          changeItem: args.changeItem,
        ),
      );
    },
    LoginRoute.name: (routeData) {
      return _i9.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i6.LoginPage(),
      );
    },
    HomeRoute.name: (routeData) {
      final args =
          routeData.argsAs<HomeRouteArgs>(orElse: () => const HomeRouteArgs());
      return _i9.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i7.HomePage(key: args.key),
      );
    },
    ChildRoute.name: (routeData) {
      final args = routeData.argsAs<ChildRouteArgs>();
      return _i9.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i8.ChildPage(
          key: args.key,
          childId: args.childId,
        ),
      );
    },
  };
}

/// generated route for
/// [_i1.RegisterPage]
class RegisterRoute extends _i9.PageRouteInfo<void> {
  const RegisterRoute({List<_i9.PageRouteInfo>? children})
      : super(
          RegisterRoute.name,
          initialChildren: children,
        );

  static const String name = 'RegisterRoute';

  static const _i9.PageInfo<void> page = _i9.PageInfo<void>(name);
}

/// generated route for
/// [_i2.AddBathPage]
class AddBathRoute extends _i9.PageRouteInfo<AddBathRouteArgs> {
  AddBathRoute({
    _i10.Key? key,
    _i11.BathItem? bathItem,
    List<_i9.PageRouteInfo>? children,
  }) : super(
          AddBathRoute.name,
          args: AddBathRouteArgs(
            key: key,
            bathItem: bathItem,
          ),
          initialChildren: children,
        );

  static const String name = 'AddBathRoute';

  static const _i9.PageInfo<AddBathRouteArgs> page =
      _i9.PageInfo<AddBathRouteArgs>(name);
}

class AddBathRouteArgs {
  const AddBathRouteArgs({
    this.key,
    this.bathItem,
  });

  final _i10.Key? key;

  final _i11.BathItem? bathItem;

  @override
  String toString() {
    return 'AddBathRouteArgs{key: $key, bathItem: $bathItem}';
  }
}

/// generated route for
/// [_i3.ChildrenPage]
class ChildrenRoute extends _i9.PageRouteInfo<ChildrenRouteArgs> {
  ChildrenRoute({
    _i10.Key? key,
    List<_i9.PageRouteInfo>? children,
  }) : super(
          ChildrenRoute.name,
          args: ChildrenRouteArgs(key: key),
          initialChildren: children,
        );

  static const String name = 'ChildrenRoute';

  static const _i9.PageInfo<ChildrenRouteArgs> page =
      _i9.PageInfo<ChildrenRouteArgs>(name);
}

class ChildrenRouteArgs {
  const ChildrenRouteArgs({this.key});

  final _i10.Key? key;

  @override
  String toString() {
    return 'ChildrenRouteArgs{key: $key}';
  }
}

/// generated route for
/// [_i4.AddLunchPage]
class AddLunchRoute extends _i9.PageRouteInfo<AddLunchRouteArgs> {
  AddLunchRoute({
    _i10.Key? key,
    _i12.LunchItem? lunchItem,
    List<_i9.PageRouteInfo>? children,
  }) : super(
          AddLunchRoute.name,
          args: AddLunchRouteArgs(
            key: key,
            lunchItem: lunchItem,
          ),
          initialChildren: children,
        );

  static const String name = 'AddLunchRoute';

  static const _i9.PageInfo<AddLunchRouteArgs> page =
      _i9.PageInfo<AddLunchRouteArgs>(name);
}

class AddLunchRouteArgs {
  const AddLunchRouteArgs({
    this.key,
    this.lunchItem,
  });

  final _i10.Key? key;

  final _i12.LunchItem? lunchItem;

  @override
  String toString() {
    return 'AddLunchRouteArgs{key: $key, lunchItem: $lunchItem}';
  }
}

/// generated route for
/// [_i5.AddChangePage]
class AddChangeRoute extends _i9.PageRouteInfo<AddChangeRouteArgs> {
  AddChangeRoute({
    _i10.Key? key,
    _i13.ChangeItem? changeItem,
    List<_i9.PageRouteInfo>? children,
  }) : super(
          AddChangeRoute.name,
          args: AddChangeRouteArgs(
            key: key,
            changeItem: changeItem,
          ),
          initialChildren: children,
        );

  static const String name = 'AddChangeRoute';

  static const _i9.PageInfo<AddChangeRouteArgs> page =
      _i9.PageInfo<AddChangeRouteArgs>(name);
}

class AddChangeRouteArgs {
  const AddChangeRouteArgs({
    this.key,
    this.changeItem,
  });

  final _i10.Key? key;

  final _i13.ChangeItem? changeItem;

  @override
  String toString() {
    return 'AddChangeRouteArgs{key: $key, changeItem: $changeItem}';
  }
}

/// generated route for
/// [_i6.LoginPage]
class LoginRoute extends _i9.PageRouteInfo<void> {
  const LoginRoute({List<_i9.PageRouteInfo>? children})
      : super(
          LoginRoute.name,
          initialChildren: children,
        );

  static const String name = 'LoginRoute';

  static const _i9.PageInfo<void> page = _i9.PageInfo<void>(name);
}

/// generated route for
/// [_i7.HomePage]
class HomeRoute extends _i9.PageRouteInfo<HomeRouteArgs> {
  HomeRoute({
    _i10.Key? key,
    List<_i9.PageRouteInfo>? children,
  }) : super(
          HomeRoute.name,
          args: HomeRouteArgs(key: key),
          initialChildren: children,
        );

  static const String name = 'HomeRoute';

  static const _i9.PageInfo<HomeRouteArgs> page =
      _i9.PageInfo<HomeRouteArgs>(name);
}

class HomeRouteArgs {
  const HomeRouteArgs({this.key});

  final _i10.Key? key;

  @override
  String toString() {
    return 'HomeRouteArgs{key: $key}';
  }
}

/// generated route for
/// [_i8.ChildPage]
class ChildRoute extends _i9.PageRouteInfo<ChildRouteArgs> {
  ChildRoute({
    _i10.Key? key,
    required String childId,
    List<_i9.PageRouteInfo>? children,
  }) : super(
          ChildRoute.name,
          args: ChildRouteArgs(
            key: key,
            childId: childId,
          ),
          initialChildren: children,
        );

  static const String name = 'ChildRoute';

  static const _i9.PageInfo<ChildRouteArgs> page =
      _i9.PageInfo<ChildRouteArgs>(name);
}

class ChildRouteArgs {
  const ChildRouteArgs({
    this.key,
    required this.childId,
  });

  final _i10.Key? key;

  final String childId;

  @override
  String toString() {
    return 'ChildRouteArgs{key: $key, childId: $childId}';
  }
}
