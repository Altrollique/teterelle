import 'package:flutter/cupertino.dart';
import 'package:teterelle/models/app_credentials.dart';

class AuthManager with ChangeNotifier {
  final isAuth = ValueNotifier<bool>(false);
  AppCredentials? _appCredentials;

  AuthManager();

  AppCredentials? get appCredentials => _appCredentials;

  set appCredentials(AppCredentials? newValue) {
    _appCredentials = newValue;
    notifyListeners();
  }
}
