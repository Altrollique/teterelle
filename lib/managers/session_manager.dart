import 'package:flutter/cupertino.dart';

class SessionManager with ChangeNotifier {
  int _globalLoadingCount = 0;

  bool get globalLoading => _globalLoadingCount != 0;
  set globalLoading(bool loading) {
    if(loading) {
      _globalLoadingCount++;
    } else {
      _globalLoadingCount--;
      if(_globalLoadingCount < 0) {
        _globalLoadingCount = 0;
      }
    }
    print("_globalLoadingCount : $_globalLoadingCount");
    notifyListeners();
  }
}
