import 'package:flutter/cupertino.dart';
import 'package:teterelle/models/app_data.dart';
import 'package:teterelle/models/child.dart';

class AppDataManager with ChangeNotifier {
  int _childIndex = 0;
  Child _child = Child();
  List<Child> _children = [];

  AppDataManager();

  AppData get appData => AppData(children: children);

  set appData(AppData? appData) {
    if (appData != null) {
      children = appData.children;
    } else {
      children = [Child()];
    }
    notifyListeners();
  }

  List<Child> get children => _children;

  set children(List<Child> newValue) {
    _children = newValue;
    if (childIndex >= newValue.length) {
      childIndex = 0;
    }
    child = newValue[childIndex];
    notifyListeners();
  }

  Child get child => _child;

  set child(Child newValue) {
    _child = newValue;
    notifyListeners();
  }

  int get childIndex => _childIndex;

  set childIndex(int newValue) {
    print("new child index : $newValue");
    _childIndex = newValue;
    child = children[childIndex];
    notifyListeners();
  }
}
