import 'package:encrypt/encrypt.dart';

class CryptoService {
  String encrypt(String value, String secret) {
    final iv = IV.fromLength(16);
    final key = Key.fromUtf8(
        '${secret}y32lengthsupersecretnooneknows12'.substring(0, 32));
    final encrypter = Encrypter(AES(key));
    return encrypter.encrypt(value, iv: iv).base64;
  }

  String decrypt(String value, String secret) {
    final iv = IV.fromLength(16);
    final key = Key.fromUtf8(
        '${secret}y32lengthsupersecretnooneknows12'.substring(0, 32));
    final encrypter = Encrypter(AES(key));
    return encrypter.decrypt(Encrypted.fromBase64(value), iv: iv);
  }
}
