import 'package:localstorage/localstorage.dart';

class LocalStorageService {
  late LocalStorage storage;

  Future<LocalStorageService> init() async {
    print("Reading storage");
    storage = LocalStorage('app_storage.json');
    print("Wait storage ready");
    await storage.ready;
    print("storage is ready");
    return this;
  }

  Future<String?> getValue(String key) async {
    return await storage.getItem(key);
  }

  Future<void> setValue(String key, String value) async {
    await storage.setItem(key, value);
  }

  Future<void> deleteValue(String key) async {
    await storage.deleteItem(key);
  }
}
