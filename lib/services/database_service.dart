import 'dart:convert';

import 'package:firebase_database/firebase_database.dart';
import 'package:teterelle/models/app_data.dart';
import 'package:teterelle/services/crypto_service.dart';
import 'package:teterelle/services/serialization_service.dart';

class DatabaseService {
  final CryptoService cryptoService;
  final SerializationService serializationService;

  DatabaseService(this.cryptoService, this.serializationService);

  Future<String?> getUserData(String userId) async {
    print("Fetching data from firebase for user $userId");
    var data = await FirebaseDatabase.instance.ref("userData/$userId").get();
    if (data.exists) {
      print("data exist");
      return data.value as String;
    } else {
      print("data doesn't exist");
      return null;
    }
  }

  AppData? decryptData(String encrypted, String password) {
    try {
      String decrypted = cryptoService.decrypt(encrypted, password);
      var jsonObject = json.decode(decrypted);
      return serializationService.appDataFromStorage(jsonObject);
    } catch (error) {
      print("Error while decrypting data");
      print(error);
      return null;
    }
  }

  String? encryptData(AppData data, String password) {
    try {
      var jsonData = json.encode(serializationService.appDataToJSON(data));
      return cryptoService.encrypt(jsonData, password);
    } catch (error) {
      print("Error while encrypting data");
      print(error);
      return null;
    }
  }

  Future<void> saveData(AppData data, String userId, String password) async {
    String? encryptedData = encryptData(data, password);
    if (encryptedData != null) {
      print("Saving data to firebase for user $userId");
      await FirebaseDatabase.instance
          .ref("userData/$userId")
          .set(encryptedData);
    }
  }
}
