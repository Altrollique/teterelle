import 'package:teterelle/managers/app_data_manager.dart';
import 'package:teterelle/managers/auth_manager.dart';
import 'package:teterelle/models/app_credentials.dart';
import 'package:teterelle/models/child.dart';
import 'package:teterelle/services/database_service.dart';

class AppDataService {
  DatabaseService storageService;
  AuthManager authManager;
  AppDataManager appDataManager;

  AppDataService(this.storageService, this.authManager, this.appDataManager);

  Future<AppDataService> init() async {
    print("init AppDataService.");
    await readAppDataFromDB();
    print("init done.");
    return this;
  }

  Future<void> readAppDataFromDB() async {
    try {
      print("readAppDataFromDB");
      AppCredentials? creds = authManager.appCredentials;
      if (creds != null) {
        print("creds are present");
        String userId = creds.userId;
        String password = creds.password;

        String? encrypted = await storageService.getUserData(userId);
        if (encrypted != null) {
          print("got encrypted data");
          appDataManager.appData =
              storageService.decryptData(encrypted, password);
        }
      } else {
        print("creds are not presents");
      }
    } catch (error) {
      print("Error while reading from DB");
      print(error);
    }
  }

  Future<void> writeAppDataToDB() async {
    try {
      print("writeAppDataToDB");
      AppCredentials? creds = authManager.appCredentials;
      if (creds != null) {
        print("creds are present");
        String userId = creds.userId;
        String password = creds.password;

        print("saving to DB");
        await storageService.saveData(appDataManager.appData, userId, password);
      } else {
        print("creds are not presents");
      }
    } catch (error) {
      print("Error while writing to DB");
      print(error);
    }
  }

  Child? getChildById(String? id) {
    for (Child child in appDataManager.children) {
      if (child.id == id) {
        return child;
      }
    }
    return null;
  }

  int getChildIndexByChildId(String id) {
    print("getChildIndexByChildId");
    return appDataManager.children.indexWhere((element) {
      print("${element.id} == $id => ${element.id == id}");
      return element.id == id;
    });
  }

  Child getChildByChildId(String id) {
    print("getChildByChildId");
    return appDataManager.children.firstWhere((element) {
      print("${element.id} == $id => ${element.id == id}");
      return element.id == id;
    }, orElse: () {
      print("no match found, returning default");
      return Child();
    });
  }

  void updateSelectedChild(Child child) {
    List<Child> result = [...appDataManager.children];
    result.replaceRange(
        appDataManager.childIndex, appDataManager.childIndex + 1, [child]);
    appDataManager.children = result;
  }

  void updateChildById(Child child, int childId) {
    List<Child> result = [...appDataManager.children];
    result.replaceRange(childId, childId + 1, [child]);
    print("Update children with child with name ${child.name}");
    appDataManager.children = result;
  }

  void addChild(Child child) {
    List<Child> result = [...appDataManager.children];
    result.add(child);
    appDataManager.children = result;
  }

  void removeChild(String id) {
    List<Child> result = [...appDataManager.children];
    print("nb item state = ${result.length}");
    result = result.where((child) => child.id != id).toList();
    print("nb item state = ${result.length}");
    appDataManager.children = result;
    appDataManager.childIndex = 0;
  }
}
