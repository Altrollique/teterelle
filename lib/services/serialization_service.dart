import 'package:flutter/material.dart';
import 'package:teterelle/models/app_data.dart';
import 'package:teterelle/models/bath_item.dart';
import 'package:teterelle/models/change_item.dart';
import 'package:teterelle/models/child.dart';
import 'package:teterelle/models/enums/history_item_type.dart';
import 'package:teterelle/models/enums/lunch_type.dart';
import 'package:teterelle/models/history_item.dart';
import 'package:teterelle/models/lunch_item.dart';
import 'package:uuid/uuid.dart';

class SerializationService {
  AppData appDataFromStorage(Map<String, dynamic> m) {
    List<dynamic> childrenJson = m['children'] ?? [];
    List<dynamic>? historyJson = m['history'];

    //@deprecated: do not handle m['history'] in the future
    List<Child> children;
    if (historyJson != null) {
      List<HistoryItem> history = List<HistoryItem>.from(
          historyJson.map((item) => historyItemFromStorage(item)));
      history.sort((a, b) => a.date.millisecondsSinceEpoch
          .compareTo(b.date.millisecondsSinceEpoch));
      children = [Child(name: "Bébé", history: history)];
    } else {
      children = [for (final child in childrenJson) childFromStorage(child)];
      children =
          List<Child>.from(childrenJson.map((item) => childFromStorage(item)));
    }

    return AppData(children: children);
  }

  Child childFromStorage(Map<String, dynamic> m) {
    String id = m['id'] ?? Uuid().v4();
    String name = m['name'] ?? "Bébé";
    List<HistoryItem> history = List<HistoryItem>.from(
        m['history'].map((item) => historyItemFromStorage(item)));
    history.sort((a, b) =>
        a.date.millisecondsSinceEpoch.compareTo(b.date.millisecondsSinceEpoch));
    return Child(id: id, name: name, history: history);
  }

  HistoryItem historyItemFromStorage(Map<String, dynamic> m) {
    var year = m["a"] ?? m["year"];
    year ??= DateTime.now().year;
    var month = m["b"] ?? m["month"];
    month ??= DateTime.now().month;
    var day = m["c"] ?? m["day"];
    day ??= DateTime.now().day;
    var hour = m["d"] ?? m["hour"];
    hour ??= DateTime.now().hour;
    var minute = m["e"] ?? m["minute"];
    minute ??= DateTime.now().minute;
    var uuid = m["f"] ?? m["uuid"];
    uuid ??= Uuid().v4();
    var typeValue = m["g"] ?? m["type"] ?? 0;
    HistoryItemType type;
    if (typeValue is String) {
      type = HistoryItemType.values.byName(typeValue);
    } else {
      type = HistoryItemType.values[typeValue];
    }
    var date = DateTime(year, month, day, hour, minute);

    switch (type) {
      case HistoryItemType.lunch:
        var lunchType =
            LunchType.values.byName(m["lunchType"] ?? LunchType.feeding.name);
        var duration = m["h"] ?? m["duration"];
        duration ??= 10;
        var isLeft =
            (m["i"] ?? m["isLeft"]) == 1 || (m["i"] ?? m["isLeft"]) == true;
        var biberonMl = m["j"] ?? m["biberonMl"];
        biberonMl ??= 0;
        if (biberonMl > 0) lunchType = LunchType.bottle;
        return LunchItem(uuid, date, lunchType, duration, isLeft, biberonMl);
      case HistoryItemType.change:
        var isPee =
            (m["h"] ?? m["isPee"]) == 1 || (m["h"] ?? m["isPee"]) == true;
        var isPoop =
            (m["i"] ?? m["isPoop"]) == 1 || (m["i"] ?? m["isPoop"]) == true;
        var isVitamin = (m["j"] ?? m["isVitamin"]) == 1 ||
            (m["j"] ?? m["isVitamin"]) == true;
        var isCord =
            (m["k"] ?? m["isCord"]) == 1 || (m["k"] ?? m["isCord"]) == true;
        return ChangeItem(uuid, date, isPee, isPoop, isVitamin, isCord);
      case HistoryItemType.bath:
        return BathItem(uuid, date);
      default:
        return HistoryItem(
            uuid, date, type, Icons.question_mark, Colors.grey.shade100);
    }
  }

  Map<String, dynamic> appDataToJSON(AppData appData) {
    Map<String, dynamic> m = {};

    m['children'] = [for (final item in appData.children) childToJSON(item)];

    return m;
  }

  Map<String, dynamic> childToJSON(Child child) {
    Map<String, dynamic> m = {};

    m['id'] = child.id;
    m['name'] = child.name;
    m['history'] = [for (final item in child.history) historyToJSON(item)];

    return m;
  }

  Map<String, dynamic> historyToJSON(HistoryItem historyItem) {
    Map<String, dynamic> m = {};

    m['year'] = historyItem.date.year;
    m['month'] = historyItem.date.month;
    m['day'] = historyItem.date.day;
    m['hour'] = historyItem.date.hour;
    m['minute'] = historyItem.date.minute;
    m['uuid'] = historyItem.uuid;
    m['type'] = HistoryItemType.values.indexOf(historyItem.type);

    switch (historyItem.type) {
      case HistoryItemType.change:
        var changeItem = historyItem as ChangeItem;
        m['isPee'] = changeItem.isPee;
        m['isPoop'] = changeItem.isPoop;
        m['isVitamin'] = changeItem.isVitamin;
        m['isCord'] = changeItem.isCord;
        break;
      case HistoryItemType.lunch:
        var lunchItem = historyItem as LunchItem;
        m['lunchType'] = lunchItem.lunchType.name;
        m['duration'] = lunchItem.duration;
        m['isLeft'] = lunchItem.isLeft;
        m['biberonMl'] = lunchItem.biberonMl;
        break;
      case HistoryItemType.bath:
        break;
    }

    return m;
  }
}
