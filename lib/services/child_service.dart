import 'package:teterelle/managers/app_data_manager.dart';
import 'package:teterelle/models/child.dart';
import 'package:teterelle/models/history_item.dart';
import 'package:teterelle/services/app_data_service.dart';

class ChildService {
  final AppDataManager appDataManager;
  final AppDataService appDataService;

  const ChildService(this.appDataManager, this.appDataService);

  void addHistory(HistoryItem item) {
    Child result = Child.copy(appDataManager.child);
    if (result.history.where((element) => element.uuid == item.uuid).isEmpty) {
      result.history = [...result.history, item];
    }
    appDataService.updateSelectedChild(result);
  }

  void editHistory(String uuid, HistoryItem item) {
    Child result = Child.copy(appDataManager.child);
    result.history = [
      for (final h in result.history)
        if (h.uuid == uuid) item else h,
    ];
    appDataService.updateSelectedChild(result);
  }

  void removeHistory(String uuid) {
    Child result = Child.copy(appDataManager.child);
    print("nb item state = ${result.history.length}");
    result.history = result.history.where((h) => h.uuid != uuid).toList();
    print("nb item state = ${result.history.length}");
    appDataService.updateSelectedChild(result);
  }
}
