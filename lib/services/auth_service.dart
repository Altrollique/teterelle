import 'package:firebase_auth/firebase_auth.dart';
import 'package:teterelle/exceptions/app_login_exception.dart';
import 'package:teterelle/managers/app_data_manager.dart';
import 'package:teterelle/managers/auth_manager.dart';
import 'package:teterelle/models/app_credentials.dart';
import 'package:teterelle/models/app_data.dart';
import 'package:teterelle/services/app_data_service.dart';
import 'package:teterelle/services/database_service.dart';
import 'package:teterelle/services/local_storage_service.dart';

class AuthService {
  final AuthManager authManager;
  final AppDataManager appDataManager;
  final AppDataService appDataService;
  final LocalStorageService localStorageService;
  final DatabaseService databaseService;

  AuthService(this.authManager, this.appDataManager, this.appDataService,
      this.localStorageService, this.databaseService);

  Future<AuthService> init() async {
    print("AuthService init");
    var userId = await localStorageService.getValue("userId");
    var email = await localStorageService.getValue("email");
    var password = await localStorageService.getValue("password");
    if (userId != null && email != null && password != null) {
      print("Creds stored in local storage");
      authManager.appCredentials = AppCredentials(userId, email, password);
      User? user = FirebaseAuth.instance.currentUser;
      if (user != null) {
        authManager.isAuth.value = true;
      } else {
        try {
          UserCredential userCreds = await FirebaseAuth.instance
              .signInWithEmailAndPassword(email: email, password: password);
          if (userCreds.user != null) {
            await localStorageService.setValue("userId", userCreds.user!.uid);
            authManager.isAuth.value = true;
          } else {
            print('Missing user in creds object');
            authManager.appCredentials = null;
            authManager.isAuth.value = false;
            await localStorageService.deleteValue("userId");
            await localStorageService.deleteValue("email");
            await localStorageService.deleteValue("password");
          }
        } catch (error) {
          print("Error while trying to authenticate user");
          print(error);
          authManager.appCredentials = null;
          authManager.isAuth.value = false;
          await localStorageService.deleteValue("userId");
          await localStorageService.deleteValue("email");
          await localStorageService.deleteValue("password");
        }
      }
    } else {
      print("no credentials in storage");
      authManager.appCredentials = null;
      authManager.isAuth.value = false;
    }

    print("init done.");
    return this;
  }

  Future<void> logout() async {
    FirebaseAuth.instance.signOut();
    await localStorageService.deleteValue("email");
    await localStorageService.deleteValue("userId");
    await localStorageService.deleteValue("password");
    authManager.appCredentials = null;
    authManager.isAuth.value = false;
    appDataManager.appData = null;
  }

  Future<void> login(String username, String password) async {
    try {
      print("login");
      String email = "$username@email.com";
      UserCredential userCreds = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      String userId = username;
      if (userCreds.user != null) {
        userId = userCreds.user!.uid;
      } else {
        print('Missing user in creds object');
        throw AppAuthException.authenticationFailed();
      }
      print("Logged in to firebase");
      var base64 = await databaseService.getUserData(userId);
      if (base64 != null) {
        var appData = databaseService.decryptData(base64, password);
        if (appData != null) {
          print("save creds to localStorage");
          await localStorageService.setValue("email", email);
          await localStorageService.setValue("userId", userId);
          await localStorageService.setValue("password", password);
          print("save creds to provider");
          authManager.appCredentials = AppCredentials(userId, email, password);
          print("merging decrypted data");
          appDataManager.appData = appData;
          print("set authed bool");
          authManager.isAuth.value = true;
          return;
        } else {
          print("No app data decrypted");
        }
      } else {
        print("No user data fetched");
      }
      throw AppAuthException.wrongCredentials();
    } on FirebaseAuthException catch (error) {
      print("Firebase Auth Error");
      print(error);
      if (error.code == 'user-not-found') {
        print('No user found for that email.');
        throw AppAuthException.wrongCredentials();
      } else if (error.code == 'wrong-password') {
        print('Wrong password provided for that user.');
        throw AppAuthException.wrongCredentials();
      }
      rethrow;
    } catch (error) {
      print("Error while login");
      print(error);
      rethrow;
    }
  }

  Future<void> register(username, password) async {
    print("register");
    try {
      String email = "$username@email.com";
      UserCredential userCreds =
          await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      String userId = username;
      if (userCreds.user != null) {
        userId = userCreds.user!.uid;
      } else {
        throw Exception('Missing user in creds object');
      }
      print("User created to firebase");

      try {
        appDataManager.appData = AppData();
        print("Saving creds");
        await localStorageService.setValue("email", email);
        await localStorageService.setValue("userId", userId);
        await localStorageService.setValue("password", password);
        AppCredentials creds = AppCredentials(userId, email, password);

        authManager.appCredentials = creds;

        print("writing data to DB");
        await appDataService.writeAppDataToDB();
        await appDataService.readAppDataFromDB();
        print("All done, user is authed");
        authManager.isAuth.value = true;
      } catch (error) {
        print("Error while creating to database");
        print(error);
        await userCreds.user!.delete();
        throw AppAuthException.dataNotCreated();
      }
    } on FirebaseAuthException catch (error) {
      print("Error while firebase register");
      print(error);
      if (error.code == 'weak-password') {
        print('The password provided is too weak.');
        throw AppAuthException.weakPassword();
      } else if (error.code == 'email-already-in-use') {
        print('The account already exists for that email.');
        throw AppAuthException.usernameTaken();
      } else {
        rethrow;
      }
    } catch (error) {
      print("Error while register");
      print(error);
      rethrow;
    }
  }
}
