import 'package:flutter/material.dart';

import 'child.dart';

class AppData with ChangeNotifier {
  List<Child> _children;

  AppData({List<Child>? children})
      : _children = (children ?? []).isNotEmpty ? children! : [Child()];

  AppData.copy(AppData from)
      : _children = from._children;

  List<Child> get children => [..._children];
  set children(List<Child> newValue) {
    _children = [...newValue];
    notifyListeners();
  }
}
