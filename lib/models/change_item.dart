import 'package:flutter/material.dart';
import 'package:teterelle/models/history_item.dart';
import 'package:teterelle/models/enums/history_item_type.dart';

class ChangeItem extends HistoryItem {
  late bool _isPee;
  late bool _isPoop;
  late bool _isVitamin;
  late bool _isCord;
  static IconData defaultIcon = Icons.baby_changing_station;
  static Color defaultColor = Colors.green.shade100;

  ChangeItem(uuid, date, this._isPee, this._isPoop, this._isVitamin, this._isCord)
      : super(uuid, date, HistoryItemType.change, ChangeItem.defaultIcon,
            ChangeItem.defaultColor);

  @override
  Map<String, dynamic> toJSONEncodable() {
    Map<String, dynamic> m = super.toJSONEncodable();

    m['isPee'] = _isPee;
    m['isPoop'] = _isPoop;
    m['isVitamin'] = _isVitamin;
    m['isCord'] = _isCord;

    return m;
  }

  bool get isPee => _isPee;
  set isPee(bool newValue) {
    _isPee = newValue;
    notifyListeners();
  }

  bool get isPoop => _isPoop;
  set isPoop(bool newValue) {
    _isPoop = newValue;
    notifyListeners();
  }

  bool get isVitamin => _isVitamin;
  set isVitamin(bool newValue) {
    _isVitamin = newValue;
    notifyListeners();
  }

  bool get isCord => _isCord;
  set isCord(bool newValue) {
    _isCord = newValue;
    notifyListeners();
  }
}
