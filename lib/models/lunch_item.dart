import 'package:flutter/material.dart';
import 'package:teterelle/models/enums/lunch_type.dart';
import 'package:teterelle/models/history_item.dart';
import 'package:teterelle/models/enums/history_item_type.dart';

class LunchItem extends HistoryItem {
  late LunchType _lunchType;
  late int _duration;
  late int _biberonMl;
  late bool _isLeft;
  static IconData defaultIcon = Icons.fastfood;
  static Color defaultColor = Colors.yellow.shade100;
  static Color colorFeeding = Colors.yellow.shade100;
  static Color colorBottle = Colors.amber.shade100;

  static Color getColor(LunchType type) {
    switch (type) {
      case LunchType.feeding:
        return LunchItem.colorFeeding;
      case LunchType.bottle:
        return LunchItem.colorBottle;
    }
  }

  LunchItem(
      uuid, date, this._lunchType, this._duration, this._isLeft, this._biberonMl)
      : super(uuid, date, HistoryItemType.lunch, LunchItem.defaultIcon,
            LunchItem.getColor(_lunchType));

  @override
  Map<String, dynamic> toJSONEncodable() {
    Map<String, dynamic> m = super.toJSONEncodable();

    m['lunchType'] = _lunchType.name;
    m['duration'] = _duration;
    m['isLeft'] = _isLeft;
    m['biberonMl'] = _biberonMl;

    return m;
  }

  LunchType get lunchType => _lunchType;
  set lunchType(LunchType newValue) {
    _lunchType = newValue;
    notifyListeners();
  }

  int get duration => _duration;
  set duration(int newValue) {
    _duration = newValue;
    notifyListeners();
  }

  int get biberonMl => _biberonMl;
  set biberonMl(int newValue) {
    _biberonMl = newValue;
    notifyListeners();
  }

  bool get isLeft => _isLeft;
  set isLeft(bool newValue) {
    _isLeft = newValue;
    notifyListeners();
  }
}
