import 'package:flutter/material.dart';

class AppCredentials with ChangeNotifier {
  String _userId;
  String _email;
  String _password;

  AppCredentials(this._userId, this._email, this._password);

  String get userId => _userId;
  set userId(String newValue) {
    _userId = newValue;
    notifyListeners();
  }

  String get email => _email;
  set email(String newValue) {
    _email = newValue;
    notifyListeners();
  }

  String get password => _password;
  set password(String newValue) {
    _password = newValue;
    notifyListeners();
  }
}
