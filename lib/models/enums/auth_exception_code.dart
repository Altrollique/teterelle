enum AuthExceptionCode {
  unknown,
  authenticationFailed,
  wrongCredentials,
  dataNotCreated,
  weakPassword,
  usernameTaken,
}
