import 'package:flutter/material.dart';
import 'package:teterelle/models/enums/history_item_type.dart';

class HistoryItem with ChangeNotifier {
  String _uuid;
  DateTime _date;
  HistoryItemType _type;
  IconData _icon;
  Color _color;
  static IconData defaultIcon = Icons.question_mark;
  static Color defaultColor = Colors.grey.shade100;

  HistoryItem(this._uuid, this._date, this._type, this._icon, this._color);

  Map<String, dynamic> toJSONEncodable() {
    Map<String, dynamic> m = {};

    m['year'] = _date.year;
    m['month'] = _date.month;
    m['day'] = _date.day;
    m['hour'] = _date.hour;
    m['minute'] = _date.minute;
    m['uuid'] = _uuid;
    m['type'] = HistoryItemType.values.indexOf(_type);

    return m;
  }

  String get uuid => _uuid;
  set uuid(String newValue) {
    _uuid = newValue;
    notifyListeners();
  }

  DateTime get date => _date;
  set date(DateTime newValue) {
    _date = newValue;
    notifyListeners();
  }

  HistoryItemType get type => _type;
  set type(HistoryItemType newValue) {
    _type = newValue;
    notifyListeners();
  }

  IconData get icon => _icon;
  set icon(IconData newValue) {
    _icon = newValue;
    notifyListeners();
  }

  Color get color => _color;
  set color(Color newValue) {
    _color = newValue;
    notifyListeners();
  }
}
