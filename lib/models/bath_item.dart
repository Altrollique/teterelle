
import 'package:flutter/material.dart';
import 'package:teterelle/models/history_item.dart';
import 'package:teterelle/models/enums/history_item_type.dart';

class BathItem extends HistoryItem {
  static IconData defaultIcon = Icons.bathtub;
  static Color defaultColor = Colors.blue.shade100;

  BathItem(uuid, date) : super(uuid, date, HistoryItemType.bath, BathItem.defaultIcon, BathItem.defaultColor);
}
