import 'package:flutter/cupertino.dart';
import 'package:teterelle/models/history_item.dart';
import 'package:uuid/uuid.dart';

class Child with ChangeNotifier {
  String _id;
  String _name;
  List<HistoryItem> _history;

  Child({String? id, String? name, List<HistoryItem>? history})
      : _id = id ?? Uuid().v4(),
        _name = name ?? "Bébé",
        _history = history ?? [];

  Child.copy(Child from)
      : _history = from.history,
        _name = from.name,
        _id = from.id;

  Map<String, dynamic> toJSONEncodable() {
    Map<String, dynamic> m = {};

    m['id'] = _id;
    m['name'] = _name;
    m['history'] = [for (final item in _history) item.toJSONEncodable()];

    return m;
  }

  String get id => _id;

  String get name => _name;
  set name(String newName) {
    _name = newName;
    notifyListeners();
  }

  List<HistoryItem> get history => [..._history];
  set history(List<HistoryItem> newValue) {
    _history = [...newValue];
    _history.sort((a, b) => a.date.millisecondsSinceEpoch.compareTo(b.date.millisecondsSinceEpoch));
    notifyListeners();
  }
}
