import 'package:teterelle/models/enums/auth_exception_code.dart';

class AppAuthException implements Exception {
  AuthExceptionCode code;
  String cause;

  AppAuthException(this.cause, this.code);

  AppAuthException.authenticationFailed() : cause = "L'authentification a échoué", code = AuthExceptionCode.authenticationFailed;
  AppAuthException.wrongCredentials() : cause = "Identifiant ou mot de passe incorrect.", code = AuthExceptionCode.wrongCredentials;
  AppAuthException.dataNotCreated() : cause = "Une erreur est survenue avec la base de donnée.", code = AuthExceptionCode.dataNotCreated;
  AppAuthException.weakPassword() : cause = "Le mot de passe est trop faible", code = AuthExceptionCode.weakPassword;
  AppAuthException.usernameTaken() : cause = "Cet identifiant est déjà pris", code = AuthExceptionCode.usernameTaken;
}
