import 'package:flutter/material.dart';

class ToggleSide extends StatelessWidget {
  const ToggleSide({
    super.key,
    required this.value,
    required this.onChanged,
    this.leftLabel = "Gauche",
    this.rightLabel = "Droite",
  });

  final String leftLabel;
  final String rightLabel;
  final bool value;
  final ValueChanged<bool> onChanged;

  void _toogle(bool newValue) {
    onChanged(newValue);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        ElevatedButton.icon(
          onPressed: () {
            _toogle(true);
          },
          label: Text(leftLabel, ),
          icon: Icon(value ? Icons.check_box_outlined : Icons.check_box_outline_blank_outlined),
          style: ElevatedButton.styleFrom(
              padding: const EdgeInsets.all(10),
          ),
        ),
        SizedBox(width: 20,),
        ElevatedButton.icon(
          onPressed: () {
            _toogle(false);
          },
          label: Text(rightLabel),
          icon: Icon(value ? Icons.check_box_outline_blank_outlined : Icons.check_box_outlined),
          style: ElevatedButton.styleFrom(
              padding: const EdgeInsets.all(10)
          ),
        ),
      ],
    );
  }
}
