import 'package:flutter/material.dart';

class LastEventCard extends StatelessWidget {
  LastEventCard(
      {Key? key,
      required this.from,
      this.icon = Icons.question_mark,
      this.backgroundColor = Colors.grey,
      this.size,
      this.onTap})
      : super(key: key);

  final DateTime from;
  final IconData icon;
  final Color backgroundColor;
  final double? size;
  void Function()? onTap;

  void handleTap() {
    if (onTap != null) {
      onTap!();
    }
  }

  @override
  Widget build(BuildContext context) {
    Duration duration = DateTime.now().difference(from);
    String hoursString = "00${duration.inHours}";
    String minutesString = "00${duration.inMinutes % 60}";
    String paddedHours = hoursString.substring(hoursString.length - 2);
    String paddedMinutes = minutesString.substring(minutesString.length - 2);

    return SizedBox(
      width: size,
      child: Card(
        color: backgroundColor,
        child: InkWell(
          onTap: handleTap,
          child: Container(
            padding: EdgeInsets.fromLTRB(10, 5, 5, 10),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: Icon(icon),
                ),
                SizedBox(height: 20),
                Align(
                  alignment: Alignment.bottomLeft,
                  child: Text("$paddedHours:$paddedMinutes"),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
