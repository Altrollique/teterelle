import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:teterelle/app_router.dart';
import 'package:teterelle/app_router.gr.dart';
import 'package:teterelle/locator.dart';
import 'package:teterelle/models/bath_item.dart';
import 'package:teterelle/models/change_item.dart';
import 'package:teterelle/models/enums/lunch_type.dart';
import 'package:teterelle/models/history_item.dart';
import 'package:teterelle/models/lunch_item.dart';
import 'package:teterelle/models/enums/history_item_type.dart';

typedef OnDeleteElement = void Function(String uuid);

class HistoryElement extends StatelessWidget {
  final AppRouter appRouter = getIt<AppRouter>();

  HistoryElement({
    Key? key,
    required this.historyItem,
    required this.onDelete,
  }) : super(key: key);

  final HistoryItem historyItem;
  final OnDeleteElement onDelete;

  Widget renderLunchItem(LunchItem item) {
    List<Widget> columnChildren = [];
    switch (item.lunchType) {
      case LunchType.feeding:
        columnChildren
            .add(Text("Tétée : ${item.isLeft ? "Gauche" : "Droite"}"));
        break;
      case LunchType.bottle:
        columnChildren.add(Text("Biberon : ${item.biberonMl} mL"));
        break;
    }
    columnChildren.add(Text(DateFormat('yyyy-MM-dd kk:mm').format(item.date)));

    if (item.lunchType == LunchType.feeding) {
      columnChildren.add(Text("Durée : ${item.duration}"));
    }
    return Column(
      children: columnChildren,
    );
  }

  Widget renderBathItem(BathItem item) {
    return Column(
      children: [
        Text(DateFormat('yyyy-MM-dd kk:mm').format(item.date)),
      ],
    );
  }

  Widget renderChangeItem(ChangeItem item) {
    List<Widget> content = [];
    if (item.isPee) {
      content.add(Text("Pipi"));
    }
    if (item.isPoop) {
      if (content.isNotEmpty) {
        content.add(Text(" / "));
      }
      content.add(Text("Caca"));
    }
    if (item.isVitamin) {
      if (content.isNotEmpty) {
        content.add(Text(" / "));
      }
      content.add(Text("Vitamine"));
    }
    if (item.isCord) {
      if (content.isNotEmpty) {
        content.add(Text(" / "));
      }
      content.add(Text("Cordon"));
    }
    if (content.isEmpty) {
      content.add(Text("Rien"));
    }

    return Column(
      children: [
        Row(
          children: content,
        ),
        Text(DateFormat('yyyy-MM-dd kk:mm').format(item.date)),
      ],
    );
  }

  void onPressed(BuildContext context) {
    switch (historyItem.type) {
      case HistoryItemType.lunch:
        LunchItem lunchItem = historyItem as LunchItem;
        appRouter.push(AddLunchRoute(lunchItem: lunchItem));
        break;
      case HistoryItemType.change:
        ChangeItem changeItem = historyItem as ChangeItem;
        appRouter.push(AddChangeRoute(changeItem: changeItem));
        break;
      case HistoryItemType.bath:
        BathItem bathItem = historyItem as BathItem;
        appRouter.push(AddBathRoute(bathItem: bathItem));
        break;
      default:
        break;
    }
  }

  Widget renderElement(HistoryItem item) {
    switch (item.type) {
      case HistoryItemType.lunch:
        return renderLunchItem(item as LunchItem);
      case HistoryItemType.change:
        return renderChangeItem(item as ChangeItem);
      case HistoryItemType.bath:
        return renderBathItem(item as BathItem);
      default:
        return Placeholder();
    }
  }

  @override
  Widget build(BuildContext context) {
    IconData icon = historyItem.icon;
    Color color = historyItem.color;

    return Card(
      color: color,
      child: InkWell(
        onTap: () => onPressed(context),
        child: Row(
          children: [
            Icon(icon),
            Spacer(flex: 2),
            renderElement(historyItem),
            Spacer(flex: 2),
            IconButton(
                onPressed: () => onDelete(historyItem.uuid),
                icon: Icon(Icons.delete))
          ],
        ),
      ),
    );
  }
}
