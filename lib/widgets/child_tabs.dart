import 'package:flutter/material.dart';
import 'package:get_it_mixin/get_it_mixin.dart';
import 'package:teterelle/locator.dart';
import 'package:teterelle/managers/app_data_manager.dart';

class ChildTabs extends StatelessWidget with GetItMixin {
  final AppDataManager appDataManager = getIt<AppDataManager>();

  void handleChildSelect(int index) {
    appDataManager.childIndex = index;
  }

  @override
  Widget build(BuildContext context) {
    final childIndex = watchOnly((AppDataManager m) => m.childIndex);
    final children = watchOnly((AppDataManager m) => m.children);

    if (children.length < 2) {
      return SizedBox.shrink();
    }

    return NavigationBar(
      selectedIndex: childIndex,
      onDestinationSelected: handleChildSelect,
      destinations: [
        for (var child in children)
          NavigationDestination(
            tooltip: '',
            icon: Icon(Icons.child_care),
            label: child.name,
            selectedIcon: Icon(Icons.child_care_outlined),
          ),
      ],
    );
  }
}
