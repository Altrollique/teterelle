
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TimePicker extends StatelessWidget {
  const TimePicker({
    super.key,
    required this.value,
    required this.onChanged,
  });

  final DateTime value;
  final ValueChanged<DateTime> onChanged;

  Future<void> _selectHour(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.fromDateTime(value),
    );
    if (picked != null) {
      final DateTime newValue = DateTime(value.year, value.month, value.day, picked.hour, picked.minute);
      if (newValue != value) {
        onChanged(newValue);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(DateFormat('kk:mm').format(value)),
        const SizedBox(width: 20.0,),
        IconButton(
          onPressed: () => _selectHour(context),
          icon: Icon(Icons.access_time)
        ),
      ],
    );
  }
}
