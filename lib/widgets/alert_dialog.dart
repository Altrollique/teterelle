import 'dart:ui';

import 'package:flutter/material.dart';

class AppAlertDialog extends StatelessWidget {
  late String title;
  late String content;
  late VoidCallback continueCallBack;
  late List<AlertButton> buttons;

  AppAlertDialog(
      {String? title,
      String? content = "Content",
      required this.continueCallBack,
      List<AlertButton>? buttons}) {
    this.title = title ?? "Titre";
    this.content = content ?? "Titre";
    this.buttons = buttons != null
        ? buttons.map((b) {
            return AlertButton(
                key: b.key,
                label: b.label,
                onPressed: () {
                  if (b.onPressed != null) {
                    b.onPressed!();
                  }
                  continueCallBack();
                });
          }).toList()
        : [AlertButton(label: "Ok", onPressed: continueCallBack)];
  }

  TextStyle textStyle = TextStyle(color: Colors.black);

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 6, sigmaY: 6),
        child: AlertDialog(
          title: Text(
            title,
            style: textStyle,
          ),
          content: Text(
            content,
            style: textStyle,
          ),
          actions: buttons,
        ));
  }
}

class AlertButton extends StatelessWidget {
  const AlertButton({Key? key, this.label = "Ok", this.onPressed})
      : super(key: key);

  final String label;
  final void Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      child: Text(label),
    );
  }
}
