
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DatePicker extends StatelessWidget {
  const DatePicker({
    super.key,
    required this.value,
    required this.onChanged,
  });

  final DateTime value;
  final ValueChanged<DateTime> onChanged;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: value,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null) {
      final DateTime newValue = DateTime(picked.year, picked.month, picked.day, value.hour, value.minute);
      if (newValue != value) {
        onChanged(newValue);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(DateFormat('yyyy-MM-dd').format(value)),
        const SizedBox(width: 20.0,),
        IconButton(
          onPressed: () => _selectDate(context),
          icon: Icon(Icons.calendar_today)
        ),
      ],
    );
  }
}
