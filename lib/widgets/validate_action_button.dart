import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ValidateActionButton extends StatefulWidget {
  const ValidateActionButton({Key? key, required this.action})
      : super(key: key);
  final Future<void> Function() action;

  @override
  State<ValidateActionButton> createState() => _ValidateActionButtonState();
}

class _ValidateActionButtonState extends State<ValidateActionButton> {
  bool loading = false;

  Future<void> handleAction() async {
    if (!loading) {
      try {
        setState(() {
          loading = true;
        });

        await widget.action();
      } catch (error) {
        print(error);
      } finally {
        setState(() {
          loading = false;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    renderChild() {
      if (loading) {
        return CircularProgressIndicator(color: Colors.white,);
      } else {
        return Icon(Icons.check);
      }
    }

    return FloatingActionButton(
      onPressed: () {
        handleAction();
      },
      child: renderChild(),
    );
  }
}
