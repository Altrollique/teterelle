import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:get_it_mixin/get_it_mixin.dart';
import 'package:teterelle/app_router.dart';
import 'package:teterelle/app_router.gr.dart';
import 'package:teterelle/locator.dart';
import 'package:teterelle/managers/app_data_manager.dart';
import 'package:teterelle/models/child.dart';
import 'package:teterelle/pages/children_page.dart';
import 'package:teterelle/services/auth_service.dart';

class AppHeader extends StatelessWidget with PreferredSizeWidget, GetItMixin {
  final AppRouter appRouter = getIt<AppRouter>();
  final AuthService authService = getIt<AuthService>();

  AppHeader({super.key});

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    String childName = watchXOnly((AppDataManager appData) => appData.child, (Child child) => child.name);

    void handleChildrenClick() {
      appRouter.navigate(ChildrenRoute());
    }

    Future<void> handleDisconnect() async {
      await authService.logout();
      appRouter.navigate(LoginRoute());
    }

    return AppBar(
      title: Text(childName),
      centerTitle: true,
      backgroundColor: Theme.of(context).colorScheme.surface,
      scrolledUnderElevation: 0.0,
      actions: [
        MenuAnchor(
          builder: (context, controller, child) {
            return IconButton(
              onPressed: () {
                if (controller.isOpen) {
                  controller.close();
                } else {
                  controller.open();
                }
              },
              icon: Icon(Icons.account_circle_outlined),
              iconSize: 32,
            );
          },
          menuChildren: [
            MenuItemButton(
              leadingIcon: Icon(Icons.child_care),
              onPressed: handleChildrenClick,
              child: Text("Gérer"),
            ),
            Divider(),
            MenuItemButton(
              leadingIcon: Icon(Icons.logout),
              onPressed: handleDisconnect,
              child: Text('Déconnexion'),
            ),
          ],
        ),
      ],
    );
  }
}
