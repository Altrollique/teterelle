import 'package:flutter/material.dart';
import 'package:get_it_mixin/get_it_mixin.dart';
import 'package:teterelle/locator.dart';
import 'package:teterelle/managers/session_manager.dart';

class AppPrimaryButon extends StatelessWidget with GetItMixin {
  final SessionManager sessionManager = getIt<SessionManager>();

  AppPrimaryButon({super.key, required this.label, this.onTap});

  final Future<void> Function()? onTap;
  final String label;

  Future<void> handleTap() async {
    sessionManager.globalLoading = true;
    try {
      if (onTap != null) {
        await onTap!();
      }
    } catch (error) {
      print("Unhandled error on AppPrimaryButon action.");
      print(error);
    } finally {
      sessionManager.globalLoading = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    bool loading = watchOnly((SessionManager session) => session.globalLoading);

    if (loading) {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16.0),
        child: Center(
          child: ElevatedButton(
            onPressed: null,
            child: CircularProgressIndicator(),
          ),
        ),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16.0),
        child: Center(
          child: ElevatedButton(
            onPressed: () => handleTap(),
            child: Text(label),
          ),
        ),
      );
    }
  }
}
